# Sugarloaf

An experimental project focused on creating a modular, single-click application for deploying a versatile web application cluster. 

The project is used to showcase a blueprint for how various packages can be used to provide a single, holistic deployment kit with various fundamental features, including:

 - [ ] Kubernetes deployment onto bare metal
 - [ ] Connect to existing Kubernetes cluster with `kubeconfig`
 - [ ] Automatic certificate renewal with ACME
 - [ ] Monitoring through Prometheus
 - [ ] Logging
 - [ ] Deploy multiple dynamic sites for different domains and sub-routes

The project uses Pulumi for provisioning and is written primarily in Go for simplicity. 

## Building

Building the basic binary can be achieved by the simple `go build` command:

```shell
go build -o .bin ./...
```

To support the target environments the following can be used:

```shell
go generate ./... && \
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o .bin/darwin-amd64/ ./... && \
CGO_ENABLED=0 GOOS=darwin GOARCH=arm64 go build -o .bin/darwin-arm64/ ./... && \
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o .bin/linux-amd64/ ./... && \
CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -o .bin/linux-arm/ ./... && \ 
CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -o .bin/linux-arm64/ ./... && \ 
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o .bin/windows-amd64/ ./... && \ 
CGO_ENABLED=0 GOOS=windows GOARCH=arm go build -o .bin/windows-arm/ ./...
```

## Code quality

Formatting: 

```shell
go fmt ./...
```

Linting:

```shell
go vet ./...
```
